.PHONY: setup create-sqs create-sns create-events

## AWS CLI LocalStack endpoint configuration
AWSLOCAL = aws --endpoint-url=http://localhost:4566
#ARN=arn:aws:sns:us-east-1:000000000000
## Queue names
#ORDER_QUEUE = OrderCreated
#PAYMENT_QUEUE = PaymentQueue
#CUSTOMER_DATA_QUEUE = CustomerDataQueue
#
## Topic names
#ORDER_TOPIC = OrderStatusTopic
#PAYMENT_TOPIC = PAYMENT_TOPIC
#
## Event bridge setup
#EVENT_BUS = DeliveryServiceBus
#
#
#tools:
#	@echo "Installing tools..."
#	go get -u github.com/golang-migrate/migrate
#
#
## Default make target
#messaging-setup: create-sqs create-sns create-events
#
## Create SQS queues
#create-sqs:
#	@echo "Creating SQS Queues..."
#	$(AWSLOCAL) sqs create-queue --queue-name $(ORDER_QUEUE)
#	$(AWSLOCAL) sqs create-queue --queue-name $(PAYMENT_QUEUE)
#	$(AWSLOCAL) sqs create-queue --queue-name $(CUSTOMER_DATA_QUEUE)
#
## Create SNS topics and subscriptions to SQS queues
#create-sns:
#	@echo "Creating SNS Topics and linking to SQS..."
#	$(AWSLOCAL) sns create-topic --name $(ORDER_TOPIC)
#	$(AWSLOCAL) sns create-topic --name $(PAYMENT_TOPIC)
#
#sub-sns:
#	@echo "Subscribing SNS Topics to SQS Queues..."
#	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(ORDER_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(ORDER_QUEUE)
#	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(PAYMENT_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(PAYMENT_QUEUE)
#
## Setup EventBridge to manage events
#create-events:
#	@echo "Setting up EventBridge..."
#	$(AWSLOCAL) events create-event-bus --name $(EVENT_BUS)
#	$(AWSLOCAL) events put-rule --name OrderEventsRule --event-bus-name $(EVENT_BUS) --event-pattern "{\"source\": [\"order\"]}"
#	$(AWSLOCAL) events put-rule --name PaymentEventsRule --event-bus-name $(EVENT_BUS) --event-pattern "{\"source\": [\"payment\"]}"
#	$(AWSLOCAL) events put-targets --rule OrderEventsRule --event-bus-name $(EVENT_BUS) --targets "Id"="1","Arn"=`$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(ORDER_TOPIC)")) | .TopicArn'`
#	#$(AWSLOCAL) events put-targets --rule PaymentEventsRule --event-bus-name $(EVENT_BUS) --targets "Id"="1","Arn"=`$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(PAYMENT_TOPIC)")) | .TopicArn'`
#
## Cleanup resources (Optional)
#clean:
#	@echo "Cleaning up all resources..."
#	$(AWSLOCAL) sns delete-topic --topic-arn `$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(ORDER_TOPIC)")) | .TopicArn'`
#	$(AWSLOCAL) sns delete-topic --topic-arn `$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(PAYMENT_TOPIC)")) | .TopicArn'`
#	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(ORDER_QUEUE) | jq -r '.QueueUrl'`
#	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(PAYMENT_QUEUE) | jq -r '.QueueUrl'`
#	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(CUSTOMER_DATA_QUEUE) | jq -r '.QueueUrl'`
#	$(AWSLOCAL) events delete-rule --name OrderEventsRule --event-bus-name $(EVENT_BUS)
#	$(AWSLOCAL) events delete-rule --name PaymentEventsRule --event-bus-name $(EVENT_BUS)
#	$(AWSLOCAL) events delete-event-bus --name $(EVENT_BUS)

purge:
	$(AWSLOCAL) sqs purge-queue --queue-url https://sqs.us-east-1.amazonaws.com/000000000000/paymentCreateCommand

receive-messages:
	$(AWSLOCAL) sqs receive-message --queue-url https://sqs.us-east-1.amazonaws.com/000000000000/OrderCreated --max-number-of-messages 10 --wait-time-seconds 10

receive-messages-payment-created:
	$(AWSLOCAL) sqs receive-message --queue-url https://sqs.us-east-1.amazonaws.com/000000000000/paymentCreated --max-number-of-messages 10 --wait-time-seconds 10

receive-messages-payment-updated:
	$(AWSLOCAL) sqs receive-message --queue-url https://sqs.us-east-1.amazonaws.com/000000000000/paymentCreateCommand --max-number-of-messages 10 --wait-time-seconds 10



send-order-msg:
	$(AWSLOCAL) sns publish \
    --topic-arn arn:aws:sns:us-east-1:000000000000:ORDER_EVENTS \
    --message '{"body":{"topic":"ORDER_EVENTS","name":"order.created","contents":{"amount":6000,"order_id":"b49e46d1-42a6-4e0f-98a6-982e0890a43a","order_items":[{"name":"Cheeseburger 123","amount":1500,"quantity":2,"total_amount":3000},{"name":"Cheeseburger 123","amount":1500,"quantity":2,"total_amount":3000}]},"version":"2019-07-09","source":"3soat-9-order-api"}}' \
    --message-attributes '{"event_type":{"DataType":"String","StringValue":"order.created"}}'

	#$(AWSLOCAL) sns publish --topic-arn arn:aws:sns:us-east-1:000000000000:ORDER_EVENTS --message "order.created"