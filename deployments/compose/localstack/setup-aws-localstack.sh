#!/usr/bin/env bash
set -euo pipefail

LOCALSTACK_HOST=http://localhost:4566/
LOCALSTACK_DUMMY_ID=000000000000
AWS_REGION=us-east-1

function newQueue() {
  local QUEUE_NAME_TO_CREATE=$1
  aws --endpoint-url=${LOCALSTACK_HOST} --region=${AWS_REGION} sqs create-queue --queue-name ${QUEUE_NAME_TO_CREATE}
}

function newTopic() {
  local TOPIC_NAME_TO_CREATE=$1
  aws --endpoint-url=${LOCALSTACK_HOST} --region=${AWS_REGION} sns create-topic --name ${TOPIC_NAME_TO_CREATE}
}

function newSubscribe() {
  aws --endpoint-url=${LOCALSTACK_HOST} sns subscribe --topic-arn arn:aws:sns:us-east-1:000000000000:IFOODCARD_CARD_EVENTS --protocol sqs --notification-endpoint http://localhost:4566/000000000000/readTestSNS
}

get_all_queues() {
   aws --endpoint-url=${LOCALSTACK_HOST} --region=${AWS_REGION} sqs list-queues
}

newQueue "HAMBURGUERIA_PAYMENT_STATUS"


echo "all queues are:"
echo "$(get_all_queues)"

