DO $$
 BEGIN
    IF EXISTS (SELECT rolname FROM pg_roles where rolname like 'payments') THEN
	 SET ROLE "payments";
	end if;
  end;
$$;

SET STATEMENT_TIMEOUT TO '300s';
