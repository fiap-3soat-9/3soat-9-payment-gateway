DO $$
 BEGIN
    IF EXISTS (SELECT rolname FROM pg_roles where rolname like 'payments') THEN
	 SET ROLE "payments";
	end if;
  end;
$$;

CREATE SCHEMA IF NOT EXISTS payments;
SET STATEMENT_TIMEOUT TO '300s';
