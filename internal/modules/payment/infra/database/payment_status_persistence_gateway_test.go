package database

import (
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"payments/internal/modules/payment/domain"
	"payments/tests/mocks/db"
	"testing"
	"time"
)

func TestProductStatusRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find payment status by ID`, func(t *testing.T) {
		paymentId := uuid.NewString()

		paymentRows := sqlmock.NewRows([]string{"id", "order_id", "data", "created_at"}).
			AddRow(paymentId, uuid.New(), "", time.Now())
		mock.ExpectQuery("^SELECT \\* FROM \"payment_status\" WHERE \"payment_status\"\\.\"\\w+\" = .*").
			WillReturnRows(paymentRows)

		paymentPersistenceGateway := GetPaymentStatusPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceGateway.FindPaymentStatus(context.TODO(), uuid.MustParse(paymentId))

		assert.Nil(t, err)
		assert.NotNil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find payment status by ID`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"payment_status\" WHERE \"payment_status\"\\.\"\\w+\" = .*").
			WillReturnError(errors.New("error"))

		paymentPersistenceInstance := GetPaymentStatusPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceInstance.FindPaymentStatus(context.TODO(), uuid.New())

		assert.NotNil(t, err)
		assert.Nil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when try id not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"payment_status\" WHERE \"payment_status\"\\.\"\\w+\" = .*").
			WillReturnError(gorm.ErrRecordNotFound)

		paymentPersistenceInstance := GetPaymentStatusPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceInstance.FindPaymentStatus(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.Nil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should create payment`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"payment_status\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		paymentPersistenceGateway := GetPaymentStatusPersistenceGateway(gormDB, gormDB, zerolog.Nop())

		err := paymentPersistenceGateway.CreatePaymentStatus(context.TODO(), createPaymentStatus())
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to create payment`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"payment_status\" (.+)$").
			WillReturnError(errors.New("error"))

		paymentPersistenceGateway := GetPaymentStatusPersistenceGateway(gormDB, gormDB, zerolog.Nop())

		err := paymentPersistenceGateway.CreatePaymentStatus(context.TODO(), createPaymentStatus())
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func createPaymentStatus() domain.PaymentStatus {
	return domain.PaymentStatus{
		Id:                   uuid.New(),
		PaymentId:            uuid.New(),
		PaymentIntegrationId: uuid.New(),
		PaymentStatus:        "created",
	}
}
