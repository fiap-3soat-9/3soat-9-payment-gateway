package database

import (
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"payments/internal/modules/payment/domain"
	"payments/tests/mocks/db"
	"testing"
	"time"
)

func TestProductRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find payment by ID`, func(t *testing.T) {
		paymentId := uuid.NewString()

		paymentRows := sqlmock.NewRows([]string{"id", "order_id", "data", "created_at"}).
			AddRow(paymentId, uuid.New(), "", time.Now())
		mock.ExpectQuery("^SELECT \\* FROM \"payment\" WHERE \"payment\"\\.\"\\w+\" = .*").
			WillReturnRows(paymentRows)

		paymentPersistenceGateway := GetPaymentPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceGateway.FindById(context.TODO(), uuid.MustParse(paymentId))

		assert.Nil(t, err)
		assert.NotNil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find payment by ID`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"payment\" WHERE \"payment\"\\.\"\\w+\" = .*").
			WillReturnError(errors.New("error"))

		paymentPersistenceInstance := GetPaymentPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceInstance.FindById(context.TODO(), uuid.New())

		assert.NotNil(t, err)
		assert.Nil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when try id not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"payment\" WHERE \"payment\"\\.\"\\w+\" = .*").
			WillReturnError(gorm.ErrRecordNotFound)

		paymentPersistenceInstance := GetPaymentPersistenceGateway(gormDB, gormDB, zerolog.Nop())
		payment, err := paymentPersistenceInstance.FindById(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.Nil(t, payment)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should create payment`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"payment\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		paymentPersistenceGateway := GetPaymentPersistenceGateway(gormDB, gormDB, zerolog.Nop())

		err := paymentPersistenceGateway.Create(context.TODO(), createPayment())
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to create payment`, func(t *testing.T) {
		mock.ExpectExec("^INSERT INTO \"payment\" (.+)$").
			WillReturnError(errors.New("error"))

		paymentPersistenceGateway := GetPaymentPersistenceGateway(gormDB, gormDB, zerolog.Nop())

		err := paymentPersistenceGateway.Create(context.TODO(), createPayment())
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func createPayment() domain.Payment {
	return domain.Payment{
		Id:        uuid.New(),
		OrderId:   uuid.New(),
		Data:      "",
		CreatedAt: time.Now(),
	}
}
