package consumer

import (
	"context"
	"fmt"
	"payments/config"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/port/output"
	"payments/internal/modules/payment/usecase"
	"payments/internal/modules/payment/usecase/command"
	"sync"

	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sqs/consumer"
)

var (
	initConsumer sync.Once
	stdConsumer  *SqsConsumerCreatePaymentsMessages
)

type SqsConsumerCreatePaymentsMessages struct {
	paymentClientGateway      output.PaymentClient
	paymentPersistenceGateway output.PaymentPersistencePort
	publisherMessage          output.PaymentPublisherEvents[representations.CreatedNotifyPub]
	queueConfig               config.SQSQueueConfig
	logger                    zerolog.Logger
}

func GetPaymentConsumer(logger zerolog.Logger, paymentClientGateway output.PaymentClient, paymentPersistenceGateway output.PaymentPersistencePort, publisherMessage output.PaymentPublisherEvents[representations.CreatedNotifyPub], queueOrderCreated config.SQSQueueConfig) SqsConsumerCreatePaymentsMessages {
	initConsumer.Do(func() {
		stdConsumer = &SqsConsumerCreatePaymentsMessages{
			paymentClientGateway:      paymentClientGateway,
			paymentPersistenceGateway: paymentPersistenceGateway,
			publisherMessage:          publisherMessage,
			queueConfig:               queueOrderCreated,
			logger:                    logger,
		}
	})
	return *stdConsumer
}

func (c SqsConsumerCreatePaymentsMessages) HandlerError(ctx context.Context, errChan <-chan error) {
	for err := range errChan {
		c.logger.Error().
			Ctx(ctx).
			Err(err).
			Msg("Failed on consumer message")
	}
}

func (c SqsConsumerCreatePaymentsMessages) Handler(ctx context.Context, msg consumer.Message[command.CreatePaymentCommand]) error {
	paymentProcessor := usecase.GetCreatePaymentUseCase(c.paymentClientGateway, c.paymentPersistenceGateway, c.publisherMessage)
	commandB, err := msg.ReadBody()
	if err != nil {
		c.logger.Error().
			Ctx(ctx).
			Err(err).
			Msg("Error on deserialize message")
		return err
	}

	//msg := commandB.Body.Contents
	//commandB, _ := pkg.Deserializer[command.CreatePaymentCommand](msg.BodyString.)

	//c.logger.Info().
	//	Ctx(ctx).
	//	Str("reference_id", msg.ReferenceID).
	//	Msg("processing payment for new order created")
	fmt.Printf("processing payment for order: %s and reference: %s\n", commandB.OrderId.String(), commandB.ReferenceID)

	_, err = paymentProcessor.CreatePayment(ctx, commandB)
	if err != nil {
		c.logger.Error().
			Ctx(ctx).
			Err(err).
			Msg("Error on process message")
		return err
	}

	//c.logger.Info().
	//	Ctx(ctx).
	//	Str("reference_id", msg.ReferenceID).
	//	Msg("payment created successfully")

	fmt.Println("payment created successfully for order: " + commandB.OrderId.String())

	return err
}

func (c SqsConsumerCreatePaymentsMessages) Start() {
	queueName := c.queueConfig.Name
	consumer.NewConsumerWithErrDecorator(
		context.Background(),
		queueName,
		c.Handler,
		c.HandlerError,
	)
}
