package sns

import (
	"context"
	"payments/internal/modules/payment/port/output"

	"github.com/rs/zerolog"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sns/publisher"
)

type SNSPublisher[T any] struct {
	logger zerolog.Logger
}

func (p SNSPublisher[T]) Publish(
	ctx context.Context,
	topicName string,
	payload T,
	messageAttributes map[string]struct{ Type, Value string },
) error {
	err := publisher.NewSNSPublisher(
		context.TODO(),
		topicName,
		message.JsonSerializer[T]{Payload: payload},
	).WithMessageAttributes(
		messageAttributes,
	).Publish()
	if err != nil {
		p.logger.Error().Ctx(ctx).Err(err).Str("Error on sent notification to topic", topicName)

		return err
	}
	return nil
}

func NewPublisher[T any]() output.PaymentPublisherEvents[T] {
	return SNSPublisher[T]{
		logger: logger.Get(),
	}
}
