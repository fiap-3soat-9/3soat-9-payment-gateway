package representations

import (
	"payments/internal/modules/payment/domain/valueobjects"

	"github.com/google/uuid"
)

type CreateStatusPaymentNotifyPub struct {
	Id                   uuid.UUID           `json:"id"`
	OrderID              uuid.UUID           `json:"order_id"`
	PaymentId            uuid.UUID           `json:"payment_id"`
	PaymentIntegrationId uuid.UUID           `json:"payment_integration_id"`
	Status               valueobjects.Status `json:"status"`
}
