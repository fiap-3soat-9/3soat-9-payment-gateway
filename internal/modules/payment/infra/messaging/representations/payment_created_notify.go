package representations

import (
	"payments/internal/modules/payment/domain/valueobjects"
	"time"

	"github.com/google/uuid"
)

type CreatedNotifyPub struct {
	PaymentId   uuid.UUID           `json:"payment_id"`
	OrderId     uuid.UUID           `json:"order_id"`
	Amount      int                 `json:"amount"`
	PaymentData string              `json:"payment_data"`
	CreatedAt   time.Time           `json:"created_at"`
	Status      valueobjects.Status `json:"status"`
}
