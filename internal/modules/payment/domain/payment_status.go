package domain

import (
	"payments/internal/modules/payment/domain/valueobjects"

	"github.com/google/uuid"
)

type PaymentStatus struct {
	Id                   uuid.UUID
	PaymentId            uuid.UUID
	PaymentIntegrationId uuid.UUID
	PaymentStatus        valueobjects.Status
}

func (ps PaymentStatus) IsStatusValid(paymentStatus PaymentStatus) bool {
	validStatus := map[valueobjects.Status]bool{
		valueobjects.Created:  true,
		valueobjects.Approved: true,
		valueobjects.Rejected: true,
	}

	_, ok := validStatus[paymentStatus.PaymentStatus]
	return ok
}
