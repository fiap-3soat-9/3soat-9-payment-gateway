package input

type MessageConsumer interface {
	Start() error
}
