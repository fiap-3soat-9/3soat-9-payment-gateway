package input

import (
	"context"
	"payments/internal/modules/payment/usecase/command"
	"payments/internal/modules/payment/usecase/result"
)

type CreatePaymentPort interface {
	CreatePayment(ctx context.Context, command command.CreatePaymentCommand) (*result.PaymentProcessed, error)
}
