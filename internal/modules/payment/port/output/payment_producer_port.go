package output

import "context"

type ProducerMessagePaymentStatus interface {
	ProduceMessage(ctx context.Context, message string) error
}
