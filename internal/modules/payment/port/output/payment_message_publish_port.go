package output

import (
	"context"
)

type PaymentPublisherEvents[T any] interface {
	Publish(
		ctx context.Context,
		topicName string,
		payload T,
		messageAttributes map[string]struct{ Type, Value string },
	) error
}
