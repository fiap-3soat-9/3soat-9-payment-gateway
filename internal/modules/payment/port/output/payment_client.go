package output

import (
	"context"
	"payments/internal/modules/payment/domain"
	"payments/internal/modules/payment/usecase/command"
)

type PaymentClient interface {
	CreatePayment(ctx context.Context, command command.CreatePaymentCommand) (domain.Payment, error)
}
