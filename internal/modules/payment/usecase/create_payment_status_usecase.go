package usecase

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"payments/config"
	"payments/internal/modules/payment/domain"
	"payments/internal/modules/payment/infra/database"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/port/input"
	"payments/internal/modules/payment/port/output"

	"payments/internal/modules/payment/usecase/command"
	"sync"

	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
)

type CreatePaymentStatusUseCase struct {
	paymentStatusPersistenceGateway output.PaymentStatusPersistencePort
	paymentPublisherMessage         output.PaymentPublisherEvents[representations.CreateStatusPaymentNotifyPub]
	logger                          zerolog.Logger
}

func (ps CreatePaymentStatusUseCase) AddPaymentStatus(ctx context.Context, command command.CreatePaymentStatusCommand) error {
	paymentStatus := mapperPaymentStatusCommandToEntityPaymentStatus(command)

	if paymentStatus.IsStatusValid(paymentStatus) {

		err := ps.paymentStatusPersistenceGateway.CreatePaymentStatus(ctx, paymentStatus)
		if err != nil {
			return err
		}

		err = ps.notify(ctx, paymentStatus)
		if err != nil {
			ps.logger.Error().Ctx(ctx).Err(err).Str("Message payment Id", string(paymentStatus.PaymentId[0])).Msg(" Failed to publish event")
			return err
		}
		ps.logger.Info().Ctx(ctx).Str("Status", string(paymentStatus.PaymentStatus)).Msg("Status publicado!")
	} else {
		return errors.New("invalid-status")
	}
	return nil
}

var (
	processPaymentStatusUseCase     CreatePaymentStatusUseCase
	processPaymentStatusUseCaseOnce sync.Once
)

func GetCreatePaymentStatusUseCase(paymentStatusPersistenceGateway output.PaymentStatusPersistencePort, paymentPublisherMessage output.PaymentPublisherEvents[representations.CreateStatusPaymentNotifyPub],
	logger zerolog.Logger) input.CreatePaymentStatusPort {
	processPaymentStatusUseCaseOnce.Do(func() {
		processPaymentStatusUseCase = CreatePaymentStatusUseCase{paymentStatusPersistenceGateway: paymentStatusPersistenceGateway, paymentPublisherMessage: paymentPublisherMessage, logger: logger}
	})
	return processPaymentStatusUseCase
}

func (ps CreatePaymentStatusUseCase) notify(ctx context.Context, paymentData domain.PaymentStatus) error {
	paymentTopic := config.GetConfig().SNS.Topics.Payment

	readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")
	paymentPersistence := database.GetPaymentPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

	paymentResult, _ := paymentPersistence.FindById(ctx, paymentData.PaymentId)

	messageAttributes := map[string]struct{ Type, Value string }{
		"event_type": {Type: "String", Value: fmt.Sprintf("%s.%s", "payment", string(paymentData.PaymentStatus))},
	}

	messageTemplate := *message.NewMessageWrapper[representations.CreateStatusPaymentNotifyPub]().
		WithBody(representations.CreateStatusPaymentNotifyPub{
			Id:                   paymentData.Id,
			OrderID:              paymentResult.OrderId,
			PaymentId:            paymentData.PaymentId,
			PaymentIntegrationId: paymentData.PaymentIntegrationId,
			Status:               paymentData.PaymentStatus}).
		WithTopic(paymentTopic.Name).
		WithSource(config.GetConfig().Application.Name).
		WithTopicEvent(representations.PaymentStatus)

	err := ps.paymentPublisherMessage.Publish(
		ctx,
		paymentTopic.Name,
		messageTemplate.Body.Contents,
		messageAttributes,
	)
	if err != nil {
		return err
	}
	return nil
}

func mapperPaymentStatusCommandToEntityPaymentStatus(command command.CreatePaymentStatusCommand) domain.PaymentStatus {
	return domain.PaymentStatus{
		Id:                   command.Id,
		PaymentId:            command.PaymentId,
		PaymentIntegrationId: command.ExternalReference,
		PaymentStatus:        command.Status,
	}
}
