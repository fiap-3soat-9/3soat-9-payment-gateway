package usecase

import (
	"context"
	"fmt"
	"payments/config"
	"payments/internal/modules/payment/domain"
	"payments/internal/modules/payment/domain/valueobjects"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/port/input"
	"payments/internal/modules/payment/port/output"
	"payments/internal/modules/payment/usecase/command"
	"payments/internal/modules/payment/usecase/result"

	"sync"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
)

var (
	processPaymentUseCase     CreatePaymentUseCase
	processPaymentUseCaseOnce sync.Once
)

type CreatePaymentUseCase struct {
	paymentClientGateway      output.PaymentClient
	paymentPersistenceGateway output.PaymentPersistencePort
	paymentPublisherMessage   output.PaymentPublisherEvents[representations.CreatedNotifyPub]
}

func (p CreatePaymentUseCase) CreatePayment(ctx context.Context, command command.CreatePaymentCommand) (*result.PaymentProcessed, error) {
	paymentData, err := p.paymentClientGateway.CreatePayment(ctx, command)
	if err != nil {
		p.notify(ctx, paymentData, command, valueobjects.Failed)
		return nil, err
	}

	errPersistence := p.paymentPersistenceGateway.Create(ctx, paymentData)
	if errPersistence != nil {
		p.notify(ctx, paymentData, command, valueobjects.Failed)
		return nil, errPersistence
	}

	nctx := context.WithValue(context.WithValue(ctx, "order_id", command.OrderId.String()), "reference_id", command.OrderId.String())

	errEventPublish := p.notify(nctx, paymentData, command, valueobjects.Created)

	if errEventPublish != nil {
		return nil, errEventPublish
	}

	return mapperPaymentEntityToPaymentProcessed(&paymentData), nil
}

func (p CreatePaymentUseCase) notify(ctx context.Context, paymentData domain.Payment, command command.CreatePaymentCommand, status valueobjects.Status) error {
	paymentTopic := config.GetConfig().SNS.Topics.Payment
	messageAttributes := map[string]struct{ Type, Value string }{
		"event_type": {Type: "String", Value: fmt.Sprintf("%s.%s", "payment", string(status))},
	}

	messageTemplate := *message.NewMessageWrapper[representations.CreatedNotifyPub]().
		WithBody(representations.CreatedNotifyPub{PaymentId: paymentData.Id, OrderId: paymentData.OrderId, Amount: command.Amount, PaymentData: paymentData.Data, CreatedAt: paymentData.CreatedAt, Status: status}).
		WithTopic(paymentTopic.Name).
		WithSource(config.GetConfig().Application.Name).
		WithTopicEvent(string(status))

	//marshal, err := json.Marshal(messageTemplate)
	//if err != nil {
	//	return err
	//}
	//
	//fmt.Println(string(marshal))

	fmt.Printf("send event: %s to topic: %s\n", fmt.Sprintf("%s.%s", "payment", string(status)), paymentTopic.Name)
	err := p.paymentPublisherMessage.Publish(
		ctx,
		paymentTopic.Name,
		messageTemplate.Body.Contents,
		messageAttributes,
	)
	if err != nil {
		return err
	}
	return nil
}

func GetCreatePaymentUseCase(paymentClientGateway output.PaymentClient, paymentPersistenceGateway output.PaymentPersistencePort, paymentPublisherMessage output.PaymentPublisherEvents[representations.CreatedNotifyPub]) input.CreatePaymentPort {
	processPaymentUseCaseOnce.Do(func() {
		processPaymentUseCase = CreatePaymentUseCase{paymentClientGateway: paymentClientGateway, paymentPersistenceGateway: paymentPersistenceGateway, paymentPublisherMessage: paymentPublisherMessage}

	})
	return processPaymentUseCase
}

func mapperPaymentEntityToPaymentProcessed(payment *domain.Payment) *result.PaymentProcessed {
	return &result.PaymentProcessed{
		PaymentId:   payment.Id,
		OrderId:     payment.OrderId,
		PaymentData: payment.Data,
		CreatedAt:   payment.CreatedAt,
	}
}
