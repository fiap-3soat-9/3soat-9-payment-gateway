package result

import (
	"time"

	"github.com/google/uuid"
)

type PaymentProcessed struct {
	PaymentId   uuid.UUID `json:"payment_id"`
	OrderId     uuid.UUID `json:"order_id"`
	PaymentData string    `json:"payment_data"`
	CreatedAt   time.Time `json:"created_at"`
}
