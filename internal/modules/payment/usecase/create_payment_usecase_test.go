package usecase

import (
	"context"
	"errors"
	"payments/internal/modules/payment/domain"
	"payments/internal/modules/payment/domain/valueobjects"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/usecase/command"
	mocks "payments/tests/mocks/modules/payment/port/output"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestCreatePayment(t *testing.T) {

	testPayment := domain.Payment{
		Id:        uuid.New(),
		OrderId:   uuid.New(),
		Data:      "",
		CreatedAt: time.Now(),
	}

	t.Run("Success", func(t *testing.T) {
		mockPaymentClient := mocks.NewPaymentClient(t)
		mockPaymentPersistence := mocks.NewPaymentPersistencePort(t)
		mockPaymentPublisher := mocks.NewPaymentPublisherEvents[message.Message[representations.CreatedNotifyPub]](t)

		testUseCase := CreatePaymentUseCase{
			paymentClientGateway:      mockPaymentClient,
			paymentPersistenceGateway: mockPaymentPersistence,
			paymentPublisherMessage:   mockPaymentPublisher,
		}

		testCommand := command.CreatePaymentCommand{
			Amount:     0,
			OrderId:    uuid.New(),
			OrderItems: nil,
		}

		mockPaymentClient.On("CreatePayment", mock.Anything, testCommand).Return(testPayment, nil)
		mockPaymentPersistence.On("Create", mock.Anything, testPayment).Return(nil)

		paymentTopic := "PAYMENT_TOPIC"
		messageTemplate := *message.NewMessageWrapper[representations.CreatedNotifyPub]().
			WithBody(representations.CreatedNotifyPub{
				PaymentId:   testPayment.Id,
				OrderId:     testPayment.OrderId,
				Amount:      testCommand.Amount,
				PaymentData: testPayment.Data,
				CreatedAt:   testPayment.CreatedAt,
				Status:      valueobjects.Created,
			}).
			WithTopic(paymentTopic).
			WithSource("payments").
			WithTopicEvent(representations.PaymentStatus)

		messageAttributes := map[string]struct{ Type, Value string }{
			"event_type": {Type: "String", Value: string(valueobjects.Created)},
		}

		mockPaymentPublisher.On("Publish", mock.Anything, paymentTopic, messageTemplate, messageAttributes).Return(nil)

		result, err := testUseCase.CreatePayment(context.Background(), testCommand)

		assert.Nil(t, err)
		assert.NotNil(t, result)
		mockPaymentClient.AssertExpectations(t)
		mockPaymentPersistence.AssertExpectations(t)
		mockPaymentPublisher.AssertExpectations(t)
	})

	t.Run("PaymentClientError", func(t *testing.T) {
		mockPaymentClient := mocks.NewPaymentClient(t)
		mockPaymentPersistence := mocks.NewPaymentPersistencePort(t)
		mockPaymentPublisher := mocks.NewPaymentPublisherEvents[message.Message[representations.CreatedNotifyPub]](t)

		testUseCase := CreatePaymentUseCase{
			paymentClientGateway:      mockPaymentClient,
			paymentPersistenceGateway: mockPaymentPersistence,
			paymentPublisherMessage:   mockPaymentPublisher,
		}

		testCommand := command.CreatePaymentCommand{
			Amount:     0,
			OrderId:    uuid.New(),
			OrderItems: nil,
		}

		mockPaymentClient.On("CreatePayment", mock.Anything, testCommand).Return(domain.Payment{}, errors.New("payment client error"))

		result, err := testUseCase.CreatePayment(context.Background(), testCommand)

		assert.NotNil(t, err)
		assert.Nil(t, result)
		mockPaymentClient.AssertExpectations(t)
	})

	t.Run("PaymentPersistenceError", func(t *testing.T) {
		mockPaymentClient := mocks.NewPaymentClient(t)
		mockPaymentPersistence := mocks.NewPaymentPersistencePort(t)
		mockPaymentPublisher := mocks.NewPaymentPublisherEvents[message.Message[representations.CreatedNotifyPub]](t)

		testUseCase := CreatePaymentUseCase{
			paymentClientGateway:      mockPaymentClient,
			paymentPersistenceGateway: mockPaymentPersistence,
			paymentPublisherMessage:   mockPaymentPublisher,
		}

		testCommand := command.CreatePaymentCommand{
			Amount:     0,
			OrderId:    uuid.New(),
			OrderItems: nil,
		}

		mockPaymentClient.On("CreatePayment", mock.Anything, testCommand).Return(testPayment, nil)
		mockPaymentPersistence.On("Create", mock.Anything, testPayment).Return(errors.New("persistence error"))

		paymentTopic := "mock-topic" // Ajuste conforme necessário
		messageTemplate := *message.NewMessageWrapper[representations.CreatedNotifyPub]().
			WithBody(representations.CreatedNotifyPub{
				PaymentId:   testPayment.Id,
				OrderId:     testPayment.OrderId,
				PaymentData: testPayment.Data,
				CreatedAt:   testPayment.CreatedAt,
				Status:      valueobjects.Failed,
			}).
			WithTopic(paymentTopic).
			WithSource("mock-source").
			WithTopicEvent(representations.PaymentStatus)

		messageAttributes := map[string]struct{ Type, Value string }{
			"event_type": {Type: "String", Value: string(valueobjects.Failed)},
		}

		mockPaymentPublisher.On("Publish", mock.Anything, paymentTopic, messageTemplate, messageAttributes).Return(nil)

		result, err := testUseCase.CreatePayment(context.Background(), testCommand)

		assert.NotNil(t, err)
		assert.Nil(t, result)
		mockPaymentClient.AssertExpectations(t)
		mockPaymentPersistence.AssertExpectations(t)
		mockPaymentPublisher.AssertExpectations(t)
	})

	t.Run("PaymentPublishEventError", func(t *testing.T) {
		mockPaymentClient := mocks.NewPaymentClient(t)
		mockPaymentPersistence := mocks.NewPaymentPersistencePort(t)
		mockPaymentPublisher := mocks.NewPaymentPublisherEvents[message.Message[representations.CreatedNotifyPub]](t)

		testUseCase := CreatePaymentUseCase{
			paymentClientGateway:      mockPaymentClient,
			paymentPersistenceGateway: mockPaymentPersistence,
			paymentPublisherMessage:   mockPaymentPublisher,
		}

		testCommand := command.CreatePaymentCommand{
			Amount:     0,
			OrderId:    uuid.New(),
			OrderItems: nil,
		}

		mockPaymentClient.On("CreatePayment", mock.Anything, testCommand).Return(testPayment, nil)
		mockPaymentPersistence.On("Create", mock.Anything, testPayment).Return(nil)

		paymentTopic := "mock-topic" // Ajuste conforme necessário
		messageTemplate := *message.NewMessageWrapper[representations.CreatedNotifyPub]().
			WithBody(representations.CreatedNotifyPub{
				PaymentId:   testPayment.Id,
				OrderId:     testPayment.OrderId,
				PaymentData: testPayment.Data,
				CreatedAt:   testPayment.CreatedAt,
				Status:      valueobjects.Created,
			}).
			WithTopic(paymentTopic).
			WithSource("mock-source").
			WithTopicEvent(representations.PaymentStatus)

		messageAttributes := map[string]struct{ Type, Value string }{
			"event_type": {Type: "String", Value: string(valueobjects.Created)},
		}

		mockPaymentPublisher.On("Publish", mock.Anything, paymentTopic, messageTemplate, messageAttributes).Return(errors.New("Failed on publish event"))

		result, err := testUseCase.CreatePayment(context.Background(), testCommand)

		assert.NotNil(t, err)
		assert.Nil(t, result)
		mockPaymentClient.AssertExpectations(t)
		mockPaymentPersistence.AssertExpectations(t)
		mockPaymentPublisher.AssertExpectations(t)
	})
}
