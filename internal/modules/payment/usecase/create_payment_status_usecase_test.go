package usecase

import (
	"context"
	"errors"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/usecase/command"
	mocks "payments/tests/mocks/modules/payment/port/output"
	"testing"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
)

func TestAddPaymentStatus(t *testing.T) {
	logger := zerolog.Nop()
	ctx := context.TODO()

	t.Run("should successfully add payment status and produce message", func(t *testing.T) {
		paymentStatusPersistenceGateway := mocks.NewPaymentStatusPersistencePort(t)
		paymentPublisherSns := mocks.NewPaymentPublisherEvents[message.Message[representations.CreateStatusPaymentNotifyPub]](t)
		useCase := CreatePaymentStatusUseCase{
			paymentStatusPersistenceGateway: paymentStatusPersistenceGateway,
			paymentPublisherMessage:         paymentPublisherSns,
			logger:                          logger,
		}
		paymentCommand := command.CreatePaymentStatusCommand{
			Id:                uuid.New(),
			PaymentId:         uuid.New(),
			ExternalReference: uuid.New(),
			Status:            "approved",
		}
		paymentStatusPersistenceGateway.On("CreatePaymentStatus", ctx, mock.Anything).Return(nil)

		err := useCase.AddPaymentStatus(ctx, paymentCommand)

		assert.NoError(t, err)
		paymentStatusPersistenceGateway.AssertExpectations(t)
	})

	t.Run("should not add payment status because status is invalid", func(t *testing.T) {
		paymentStatusPersistenceGateway := mocks.NewPaymentStatusPersistencePort(t)
		paymentPublisherSns := mocks.NewPaymentPublisherEvents[representations.CreateStatusPaymentNotifyPub](t)

		useCase := CreatePaymentStatusUseCase{
			paymentStatusPersistenceGateway: paymentStatusPersistenceGateway,
			paymentPublisherMessage:         paymentPublisherSns,
			logger:                          logger,
		}
		paymentCommand := command.CreatePaymentStatusCommand{
			Id:                uuid.New(),
			PaymentId:         uuid.New(),
			ExternalReference: uuid.New(),
			Status:            "error",
		}

		err := useCase.AddPaymentStatus(ctx, paymentCommand)

		assert.Error(t, err)
		paymentStatusPersistenceGateway.AssertExpectations(t)
	})

	t.Run("should fail to produce message and return error", func(t *testing.T) {
		paymentStatusPersistenceGateway := mocks.NewPaymentStatusPersistencePort(t)
		paymentPublisherSns := mocks.NewPaymentPublisherEvents[representations.CreateStatusPaymentNotifyPub](t)
		useCase := CreatePaymentStatusUseCase{
			paymentStatusPersistenceGateway: paymentStatusPersistenceGateway,
			paymentPublisherMessage:         paymentPublisherSns,
			logger:                          logger,
		}
		paymentCommand := command.CreatePaymentStatusCommand{
			Id:                uuid.New(),
			PaymentId:         uuid.New(),
			ExternalReference: uuid.New(),
			Status:            "approved",
		}
		expectedError := errors.New("failed to produce message")
		paymentStatusPersistenceGateway.On("CreatePaymentStatus", ctx, mock.Anything).Return(nil)

		err := useCase.AddPaymentStatus(ctx, paymentCommand)

		assert.Error(t, err)
		assert.Equal(t, expectedError, err)
		paymentStatusPersistenceGateway.AssertCalled(t, "CreatePaymentStatus", ctx, mock.Anything)
	})
	t.Run("should fail to add payment status to persistence and return error", func(t *testing.T) {
		paymentStatusPersistenceGateway := mocks.NewPaymentStatusPersistencePort(t)
		paymentPublisherSns := mocks.NewPaymentPublisherEvents[representations.CreateStatusPaymentNotifyPub](t)
		useCase := CreatePaymentStatusUseCase{
			paymentStatusPersistenceGateway: paymentStatusPersistenceGateway,
			paymentPublisherMessage:         paymentPublisherSns,
			logger:                          logger,
		}
		paymentCommand := command.CreatePaymentStatusCommand{
			Id:                uuid.New(),
			PaymentId:         uuid.New(),
			ExternalReference: uuid.New(),
			Status:            "approved",
		}
		expectedError := errors.New("failed to add payment status")
		paymentStatusPersistenceGateway.On("CreatePaymentStatus", ctx, mock.Anything).Return(expectedError)

		err := useCase.AddPaymentStatus(ctx, paymentCommand)

		assert.Error(t, err)
		assert.Equal(t, expectedError, err)
		paymentStatusPersistenceGateway.AssertExpectations(t)
	})
}
