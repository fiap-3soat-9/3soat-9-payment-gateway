package command

import "github.com/google/uuid"

type CreatePaymentCommand struct {
	ReferenceID string       `json:"reference_id"`
	Amount      int          `json:"amount"`
	OrderId     uuid.UUID    `json:"order_id"`
	OrderItems  []OrderItems `json:"order_items"`
}

type OrderItems struct {
	Name        string `json:"name"`
	Amount      int    `json:"amount"`
	Quantity    int    `json:"quantity"`
	TotalAmount int    `json:"total_amount"`
}
