package command

import (
	"payments/internal/modules/payment/domain/valueobjects"

	"github.com/google/uuid"
)

type PaymentStatusMessage struct {
	Id        uuid.UUID
	OrderID   uuid.UUID           `json:"order_id"`
	Status    valueobjects.Status `json:"status"`
	PaymentID uuid.UUID           `json:"payment_id,omitempty"`
}
