package payment

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"payments/internal/modules/payment/port/input"
	"payments/internal/web/api/middleware"
	"payments/internal/web/api/rest/v1/payment/request"
)

type Webhook struct {
	CreatePaymentStatusUseCase input.CreatePaymentStatusPort
}

func (wh *Webhook) RegisterEchoRoutes(e *echo.Echo) {
	group := e.Group("/v1/webhook/payments_status",
		middleware.GetTraceCallsMiddlewareFunc(),
		middleware.GetLogCallsMiddlewareFunc(),
	)
	group.Add(http.MethodPost, "", wh.AddPaymentStatus)

}

// AddPaymentStatus
// @Summary     Add payment_status
// @Description Add payment_status
// @Produce      json
// @Param 		 request 	   body   request.CreatePaymentStatusRequest true "Request Body"
// @Failure      400 {object} v1.ErrorResponse
// @Failure      401 {object} v1.ErrorResponse
// @Failure      404 {object} v1.ErrorResponse
// @Failure      503 {object} v1.ErrorResponse
// @Router       /v1/payments_status [post]
func (wh *Webhook) AddPaymentStatus(ctx echo.Context) error {
	req := new(request.CreatePaymentStatusRequest)

	if errBind := ctx.Bind(req); errBind != nil {
		return ctx.JSON(http.StatusBadRequest, map[string]any{
			"code":    400,
			"message": "UNMARSHAL_ERROR",
		})
	}

	errCreated := wh.CreatePaymentStatusUseCase.AddPaymentStatus(ctx.Request().Context(), req.PaymentStatusRequestToCommand())
	if errCreated != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]any{
			"code":    http.StatusInternalServerError,
			"message": errCreated.Error(),
		})
	}
	return ctx.JSON(http.StatusOK, nil)
}
