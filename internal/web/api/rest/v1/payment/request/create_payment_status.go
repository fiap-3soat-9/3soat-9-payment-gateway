package request

import (
	"payments/internal/modules/payment/domain/valueobjects"
	"payments/internal/modules/payment/usecase/command"
	"strings"
	"time"

	"github.com/google/uuid"
)

type CreatePaymentStatusRequest struct {
	Id          uuid.UUID  `json:"id"`
	LiveMode    bool       `json:"live_mode"`
	Type        string     `json:"type"`
	DateCreated time.Time  `json:"date_created"`
	UserID      int        `json:"user_id"`
	APIVersion  string     `json:"api_version"`
	Action      string     `json:"action"`
	Data        DataStatus `json:"data"`
}
type DataStatus struct {
	Id uuid.UUID `json:"id"`
}

func (c CreatePaymentStatusRequest) GetPaymentStatus() string {
	const prefix = "payment."
	if strings.HasPrefix(c.Action, prefix) {
		return strings.TrimPrefix(c.Action, prefix)
	}
	return ""
}

func (c CreatePaymentStatusRequest) PaymentStatusRequestToCommand() command.CreatePaymentStatusCommand {
	return command.CreatePaymentStatusCommand{
		Id:                c.Id,
		ExternalReference: c.Data.Id,
		PaymentId:         c.Data.Id,
		Status:            valueobjects.Status(c.GetPaymentStatus()),
	}
}

type CreatePaymentRequest struct {
	Amount     int                `json:"amount"`
	OrderId    string             `json:"orderId"`
	OrderItems []OrderItemRequest `json:"orderItems"`
}

type OrderItemRequest struct {
	Name        string `json:"name"`
	Amount      int    `json:"amount"`
	Quantity    int    `json:"quantity"`
	TotalAmount int    `json:"totalAmount"`
}

func (cpr CreatePaymentRequest) PaymentRequestToCommand() command.CreatePaymentCommand {
	var orderItems []command.OrderItems

	for _, item := range cpr.OrderItems {
		orderItem := command.OrderItems{
			Name:        item.Name,
			Amount:      item.Amount,
			Quantity:    item.Quantity,
			TotalAmount: item.TotalAmount,
		}
		orderItems = append(orderItems, orderItem)
	}
	return command.CreatePaymentCommand{
		Amount:     cpr.Amount,
		OrderId:    uuid.MustParse(cpr.OrderId),
		OrderItems: orderItems,
	}
}
