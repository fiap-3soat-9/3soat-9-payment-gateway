package payment

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"payments/internal/modules/payment/usecase/result"
	mocks "payments/tests/mocks/modules/payment/port/input"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestPaymentApi(t *testing.T) {

	t.Run(`should find payment by id`, func(t *testing.T) {
		findPayment := mocks.NewFindPaymentPort(t)

		paymentApi := Api{
			FindPayment: findPayment,
		}

		paymentResponse := &result.PaymentProcessed{
			PaymentId:   uuid.UUID{},
			OrderId:     uuid.UUID{},
			PaymentData: "",
			CreatedAt:   time.Time{},
		}

		id := uuid.NewString()

		findPayment.On("FindPaymentById", mock.Anything, uuid.MustParse(id)).Return(paymentResponse, nil)

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/payments/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("payment")
		echoContext.SetParamValues(id)

		response := paymentApi.GetPayment(echoContext)

		findPayment.AssertExpectations(t)
		findPayment.AssertCalled(t, "FindPaymentById", mock.Anything, uuid.MustParse(id))
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when find product by invalid id`, func(t *testing.T) {
		findPayment := mocks.NewFindPaymentPort(t)

		paymentApi := Api{
			FindPayment: findPayment,
		}

		id := "error"

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/payments/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("payment")
		echoContext.SetParamValues(id)

		response := paymentApi.GetPayment(echoContext)

		findPayment.AssertExpectations(t)
		findPayment.AssertNotCalled(t, "FindPaymentById", mock.Anything, id)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return error when find product by empty id`, func(t *testing.T) {
		findPayment := mocks.NewFindPaymentPort(t)

		paymentApi := Api{
			FindPayment: findPayment,
		}

		id := ""

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/payments/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("payment")
		echoContext.SetParamValues(id)

		response := paymentApi.GetPayment(echoContext)

		findPayment.AssertExpectations(t)
		findPayment.AssertNotCalled(t, "FindPaymentById", mock.Anything, id)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find payment by id`, func(t *testing.T) {
		findPayment := mocks.NewFindPaymentPort(t)

		paymentApi := Api{
			FindPayment: findPayment,
		}

		id := uuid.NewString()

		findPayment.On("FindPaymentById", mock.Anything, uuid.MustParse(id)).Return(nil, errors.New("error"))

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/payments/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("payment")
		echoContext.SetParamValues(id)

		response := paymentApi.GetPayment(echoContext)

		findPayment.AssertExpectations(t)
		findPayment.AssertCalled(t, "FindPaymentById", mock.Anything, uuid.MustParse(id))
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should return empty when find payment by unknown id`, func(t *testing.T) {
		findPayment := mocks.NewFindPaymentPort(t)

		paymentApi := Api{
			FindPayment: findPayment,
		}

		id := uuid.NewString()

		findPayment.On("FindPaymentById", mock.Anything, uuid.MustParse(id)).Return(nil, nil)

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/payments/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("payment")
		echoContext.SetParamValues(id)

		response := paymentApi.GetPayment(echoContext)

		findPayment.AssertExpectations(t)
		findPayment.AssertCalled(t, "FindPaymentById", mock.Anything, uuid.MustParse(id))
		assert.Nil(t, response)
		assert.Equal(t, http.StatusNoContent, echoContext.Response().Status)
	})

}
