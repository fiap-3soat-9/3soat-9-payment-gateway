package response

import (
	"time"

	"github.com/google/uuid"
)

type PaymentProcessedResponse struct {
	PaymentId   uuid.UUID `json:"paymend_id"`
	OrderId     uuid.UUID `json:"order_id"`
	PaymentData string    `json:"payment_data"`
	CreatedAt   time.Time `json:"created_at"`
}
