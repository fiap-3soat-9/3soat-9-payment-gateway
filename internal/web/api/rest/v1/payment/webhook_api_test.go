package payment

import (
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	mocks "payments/tests/mocks/modules/payment/port/input"
	"strings"
	"testing"
)

func TestWebhookApi(t *testing.T) {
	t.Run(`should create payment status`, func(t *testing.T) {
		createPaymentStatusUseCase := mocks.NewCreatePaymentStatusPort(t)

		webhook := Webhook{
			CreatePaymentStatusUseCase: createPaymentStatusUseCase,
		}

		requestBody := `{
		  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
		  "live_mode": true,
		  "type": "payment_status",
		  "date_created": "2024-05-18T15:34:56Z",
		  "user_id": 12345,
		  "api_version": "v1",
		  "action": "update",
		  "data": {
			"id": "b93cbaac-5b1d-4b5e-a918-4775b3f7c507"
	  		}
		}`

		createPaymentStatusUseCase.On("AddPaymentStatus", mock.Anything, mock.Anything).Return(nil)

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/webhook/payments_status"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := webhook.AddPaymentStatus(echoContext)

		createPaymentStatusUseCase.AssertExpectations(t)
		createPaymentStatusUseCase.AssertCalled(t, "AddPaymentStatus", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return 400 when create product when payload is invalid`, func(t *testing.T) {
		createPaymentStatusUseCase := mocks.NewCreatePaymentStatusPort(t)

		webhook := Webhook{
			CreatePaymentStatusUseCase: createPaymentStatusUseCase,
		}

		requestBody := `{
		  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
		  "live_mode": "teste",
		  "type": "payment_status",
		  "date_created": "2024-05-18T15:34:56Z",
		  "user_id": 12345,
		  "api_version": "v1",
		  "action": "update",
		  "data": {
			"id": "b93cbaac-5b1d-4b5e-a918-4775b3f7c507"
	  		}
		}`

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/webhook/payments_status"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := webhook.AddPaymentStatus(echoContext)

		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)

		createPaymentStatusUseCase.AssertExpectations(t)
		createPaymentStatusUseCase.AssertNotCalled(t, "AddPaymentStatus", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when try to create payment status`, func(t *testing.T) {
		createPaymentStatusUseCase := mocks.NewCreatePaymentStatusPort(t)

		webhook := Webhook{
			CreatePaymentStatusUseCase: createPaymentStatusUseCase,
		}

		requestBody := `{
		  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
		  "live_mode": true,
		  "type": "payment_status",
		  "date_created": "2024-05-18T15:34:56Z",
		  "user_id": 12345,
		  "api_version": "v1",
		  "action": "update",
		  "data": {
			"id": "b93cbaac-5b1d-4b5e-a918-4775b3f7c507"
	  		}
		}`

		createPaymentStatusUseCase.On("AddPaymentStatus", mock.Anything, mock.Anything).Return(errors.New("failed"))

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/webhook/payments_status"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := webhook.AddPaymentStatus(echoContext)

		createPaymentStatusUseCase.AssertExpectations(t)
		createPaymentStatusUseCase.AssertCalled(t, "AddPaymentStatus", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})
}
