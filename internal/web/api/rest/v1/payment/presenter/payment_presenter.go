package presenter

import (
	"payments/internal/modules/payment/usecase/result"
	"payments/internal/web/api/rest/v1/payment/response"
)

func CreatePaymentResponseFromResult(result result.PaymentProcessed) response.PaymentProcessedResponse {
	return response.PaymentProcessedResponse{
		PaymentId:   result.PaymentId,
		OrderId:     result.OrderId,
		PaymentData: result.PaymentData,
		CreatedAt:   result.CreatedAt,
	}
}
