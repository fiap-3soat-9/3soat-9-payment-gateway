package payment

import (
	"payments/internal/modules/payment/port/input"
	"payments/internal/web/api/middleware"
	"payments/internal/web/api/rest/v1/payment/presenter"

	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

type Api struct {
	FindPayment input.FindPaymentPort
}

func (c *Api) RegisterEchoRoutes(e *echo.Echo) {
	group := e.Group("/v1/payments",
		middleware.GetTraceCallsMiddlewareFunc(),
		middleware.GetLogCallsMiddlewareFunc(),
	)
	group.Add(http.MethodGet, "/:payment", c.GetPayment)

}

func (c *Api) GetPayment(e echo.Context) error {
	paymentPathParam := e.Param("payment")
	if paymentPathParam == "" {
		return e.JSON(http.StatusBadRequest, map[string]any{
			"code":    400,
			"message": "payment cannot be empty",
		})
	}
	paymentId, err := uuid.Parse(paymentPathParam)
	if err != nil {
		return e.JSON(http.StatusBadRequest, map[string]any{
			"code":    400,
			"message": "payment must be a uuid value",
		})
	}

	payment, err := c.FindPayment.FindPaymentById(e.Request().Context(), paymentId)

	if err != nil {
		return e.JSON(http.StatusInternalServerError, map[string]any{
			"code":    500,
			"message": err.Error(),
		})
	}

	if payment == nil {
		return e.JSON(http.StatusNoContent, nil)
	}

	return e.JSON(http.StatusOK, presenter.CreatePaymentResponseFromResult(*payment))
}
