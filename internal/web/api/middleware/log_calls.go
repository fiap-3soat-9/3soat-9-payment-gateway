package middleware

import (
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"strings"
)

var HeadersToAccept = map[string]bool{
	"X-B3-Traceid": true,
	"X-B3-Spanid":  true,
}

func GetLogCallsMiddlewareFunc() func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			l := logger.Get()

			ctx := c.Request().Context()
			headers := c.Request().Header

			for key, value := range headers {
				if HeadersToAccept[key] {
					l.UpdateContext(func(c zerolog.Context) zerolog.Context {
						return c.Str(key, strings.Join(value, ";"))
					})
				}
			}

			l.Info().
				Ctx(ctx).
				Str("method", c.Request().Method).
				Str("path", c.Request().RequestURI).
				Msg("received request")

			c.Request().WithContext(ctx)
			return next(c)
		}
	}
}
