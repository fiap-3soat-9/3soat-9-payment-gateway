package middleware

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestLogCallsMiddleware(t *testing.T) {

	t.Run(`should call log function`, func(t *testing.T) {
		logCallsMiddlewareFunction := GetLogCallsMiddlewareFunc()
		logCallsMiddlewareHandlerFunction := logCallsMiddlewareFunction(mockHandlerFunction)

		echoServer := echo.New()
		testUrl := "http://localhost:8080/customers/275b428e-663b-4268-96e8-74f1dcb1d98b/claim"
		headerB3Key := "X-B3-TraceId"
		headerB3Value := "12345"
		bodyContent := `{"token": "NjIxMTUwMTIzNDU2Nzg5MTAxMTU1MTMzMg=="}`
		body := strings.NewReader(bodyContent)
		req, _ := http.NewRequest(http.MethodPost, testUrl, body)
		req.Header.Add(headerB3Key, headerB3Value)
		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		requestContext := echoContext.Request().Context()

		err := logCallsMiddlewareHandlerFunction(echoContext)
		if err != nil {
			return
		}

		assert.Nil(t, err)
		assert.Equal(t, "POST", http.MethodPost)
		assert.NotNil(t, requestContext)
	})

}
