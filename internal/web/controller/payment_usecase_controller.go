package controller

import (
	paymentDatabase "payments/internal/modules/payment/infra/database"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/infra/messaging/sns"
	"payments/internal/modules/payment/port/input"
	paymentUseCase "payments/internal/modules/payment/usecase"
	"sync"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
)

type PaymentStatusUseCaseController struct {
	CreatePaymentStatus input.CreatePaymentStatusPort
}

var (
	paymentStatusUseCaseControllerInstance                               *PaymentStatusUseCaseController
	findPaymentUseCaseControllerInstance                                 *FindPaymentUseCaseController
	paymentStatusUseCaseControllerOnce, paymentFindUseCaseControllerOnce sync.Once
)

func GetPaymentStatusUseCaseController(readWriteDB, readOnlyDB *gorm.DB) *PaymentStatusUseCaseController {
	paymentStatusUseCaseControllerOnce.Do(func() {
		paymentStatusPersistenceGateway := paymentDatabase.GetPaymentStatusPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

		createStatusPaymentStatus := paymentUseCase.GetCreatePaymentStatusUseCase(
			paymentStatusPersistenceGateway,
			sns.NewPublisher[representations.CreateStatusPaymentNotifyPub](),
			logger.Get(),
		)

		paymentStatusUseCaseControllerInstance = &PaymentStatusUseCaseController{
			CreatePaymentStatus: createStatusPaymentStatus,
		}
	})

	return paymentStatusUseCaseControllerInstance

}

type FindPaymentUseCaseController struct {
	GetPayment input.FindPaymentPort
}

func GetFindPaymentUseCaseController(readWriteDB, readOnlyDB *gorm.DB) *FindPaymentUseCaseController {
	paymentFindUseCaseControllerOnce.Do(func() {
		paymentPersistenceGateway := paymentDatabase.GetPaymentPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

		findPayment := paymentUseCase.GetFindPaymentUseCase(paymentPersistenceGateway)

		findPaymentUseCaseControllerInstance = &FindPaymentUseCaseController{
			GetPayment: findPayment,
		}
	})

	return findPaymentUseCaseControllerInstance

}
