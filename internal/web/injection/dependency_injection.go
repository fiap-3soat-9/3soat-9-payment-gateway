package injection

import (
	"payments/config"
	"payments/internal/modules/payment/infra/client/mercadopago"
	paymentDatabase "payments/internal/modules/payment/infra/database"
	"payments/internal/modules/payment/infra/messaging/representations"
	"payments/internal/modules/payment/infra/messaging/sns"
	paymentConsumer "payments/internal/modules/payment/infra/messaging/sqs/consumer"
	"payments/internal/modules/payment/port/output"
	"payments/internal/web/api/rest/v1/payment"
	"payments/internal/web/api/swagger"
	"payments/internal/web/controller"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"gorm.io/gorm"
)

type DependencyInjection struct {
	PaymentsStatusWebhook *payment.Webhook
	PaymentApi            *payment.Api
	Swagger               *swagger.Swagger
}

func NewDependencyInjection() DependencyInjection {
	mercadoPagoClient := mercadopago.GetCreateMercadoPagoClient(
		httpclient.GetClient("mercadoPago"),
		config.GetConfig().MercadoPago,
		logger.Get(),
	)

	readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")
	paymentStatusUseCaseController := controller.GetPaymentStatusUseCaseController(readWriteDB, readOnlyDB)

	paymentFindUseCaseController := controller.GetFindPaymentUseCaseController(readWriteDB, readOnlyDB)

	registerConsumers(mercadoPagoClient, readWriteDB, readOnlyDB)

	return DependencyInjection{
		PaymentsStatusWebhook: &payment.Webhook{
			CreatePaymentStatusUseCase: paymentStatusUseCaseController.CreatePaymentStatus,
		},
		PaymentApi: &payment.Api{
			FindPayment: paymentFindUseCaseController.GetPayment,
		},
		Swagger: &swagger.Swagger{},
	}
}

func registerConsumers(mercadoPagoClient output.PaymentClient, readWriteDB, readOnlyDB *gorm.DB) {
	paymentPersistenceGateway := paymentDatabase.GetPaymentPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())
	paymentRequestConsumer := paymentConsumer.GetPaymentConsumer(logger.Get(), mercadoPagoClient, paymentPersistenceGateway, sns.NewPublisher[representations.CreatedNotifyPub](), config.GetConfig().SQS.Queues.ProcessPaymentStatusCommand)

	go paymentRequestConsumer.Start()
}
