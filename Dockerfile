FROM golang:1.22-alpine as base

WORKDIR /payments

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

# BUILDER
FROM base as builder

COPY . /payments

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -ldflags "${LDFLAGS} -s -w" -a -o payments-app /payments/cmd/main.go

FROM scratch

WORKDIR /payments

COPY --from=builder /payments/config /payments/config
COPY --from=builder /payments/payments-app /payments


EXPOSE 8080 8081


CMD ["./payments-app"]
