package config

type AwsConfig struct {
	Region          string
	AccountId       string
	Endpoint        string
	AccessKeyId     string
	SecretAccessKey string
	Profile         string
}
