package config

type SQSQueues struct {
	Queues Queues
}

type Queues struct {
	ProcessPaymentStatusCommand SQSQueueConfig
}

type SQSQueueConfig struct {
	Name string
}

type SNSConfig struct {
	Topic
}

type Topic struct {
	Name string
	Arn  string
}

type SNSTopics struct {
	Topics Topics
}

type Topics struct {
	Payment Topic
}
