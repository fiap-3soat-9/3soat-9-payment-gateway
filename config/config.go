package config

type ApplicationConfig struct {
	Application Application
	SNS         SNSTopics
	SQS         SQSQueues
	MercadoPago MercadoPago
}

var configInstance ApplicationConfig

func SetConfig(c ApplicationConfig) {
	configInstance = c
}

func GetConfig() ApplicationConfig {
	return configInstance
}
