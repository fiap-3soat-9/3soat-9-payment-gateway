package main

import (
	"os"
	"payments/config"
	_ "payments/docs"
	"payments/internal/web/injection"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpserver"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/validation"
)

// @title fiap-3soat-9-ms-payments - Grupo 9
// @version 1.0
// @description Projeto de auto atendimento para fiap-3soat-9-ms-payments
func main() {

	os.Setenv("GO_PROFILE", "local")
	starter.Initialize()

	var serviceConfig config.ApplicationConfig
	err := starter.UnmarshalConfig(&serviceConfig)
	if err != nil {
		panic("error on unmarshall configs")
	}
	config.SetConfig(serviceConfig)
	httpclient.Initialize()

	sql.Initialize()

	dependencyInjection := injection.NewDependencyInjection()

	server := httpserver.Builder().
		WithConfig(starter.GetHttpServerConfig()).
		WithHealthCheck(sql.GetHealthChecker()).
		WithControllers(injection.GetAllApis(dependencyInjection)...).
		WithValidator(validation.GetEchoValidator()).
		Build()

	server.Listen()
}
